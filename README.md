# README #

### What is this repository for? ###

Export Your Magento 1 product data as a CSV that is compatible with Magento 2 standart product importer.
By default A CSV is exported into inc/old_products.csv

This CSV should then be compatible with the Magento 2 product import tool.

Version 1.1

### How do I get set up? ###

Upload to the root of your webserver and access www.yourwebsite.com/m2exporter from a browser

in version 1.0 all config can be acheived by editing the file inc/magento_exporter.php

To use this tool you will need a good knowledge of PHP.

As always, back up your site before use and proceed with caution.




### Contribution guidelines ###

Feel free to PULL this proiject and hack it to bits. Make it do what you need it to!

### Who do I talk to? ###

Repo owner or admin
Other community or team contact

### Change Log ###

Version 1.1 - Initial Release. 
- Simple and Configurable Products import capabilities