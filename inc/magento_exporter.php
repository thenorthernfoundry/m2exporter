<?php
require_once('header.php');
require_once('magento.php');

try {
    $handle = fopen(SAVE_FEED_LOCATION, 'w');

    $heading = array('sku','store_view_code','attribute_set_code','product_type','categories','product_websites','name,description','short_description','weight','product_online','tax_class_name','visibility','price','special_price','special_price_from_date','special_price_to_date','url_key','meta_title','base_image','base_image_label','small_image','small_image_label','thumbnail_image','thumbnail_image_label','swatch_image','swatch_image_label','msrp_display_actual_price_type','country_of_manufacture','additional_attributes','qty','configurable_variations');
    $feed_line = implode(",", $heading) . "\r\n";
    fwrite($handle, $feed_line);

    //---------------------- GET THE PRODUCTS
    $products = Mage::getModel('catalog/product')->getCollection();
    //$products->setPageSize(30);
    //$products->addAttributeToFilter('status', 1); //enabled
    //$products->addAttributeToFilter('visibility', 4); //catalog, search
    $products->addAttributeToSelect('*');
    //$prodIds = $products->getAllIds();

    $counter = 0;

    foreach ($products as $product) {

    	$attributeSetName = Mage::getModel('eav/entity_attribute_set')->load($product->getAttributeSetId())->getAttributeSetName();
    	$stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
    	//$product = Mage::getModel('catalog/product');
        $product->load($productId);
        $product_data = array();

    	$product_data['sku'] = $product->getSku();
    	$product_data['store_view_code'] = '';
    	$product_data['attribute_set_code'] = $attributeSetName;
    	$product_data['product_type'] =	$product->getTypeId();
        $product_data['categories'] = '';
    	// //get the product categories
         foreach ($product->getCategoryIds() as $_categoryId) {
             $category = Mage::getModel('catalog/category')->load($_categoryId);
             $product_data['categories'] .= $category->getName() . ' / ';
         }
    	$product_data['product_websites'] =	'';
    	$product_data['name'] =	$product->getName();
    	$product_data['description'] = '"'.$product->getDescription().'"';
    	$product_data['short_description'] = '"'.$product->getShortDescription().'"';
    	$product_data['weight'] = $product->getWeight();
    	$product_data['product_online'] = 1;	
    	$product_data['tax_class_name'] = 'Taxable Goods';	
    	$product_data['visibility'] = $product->getVisibility();
    	$product_data['price'] = $product->getPrice();
    	$product_data['special_price'] = $product->getSpecialPrice();
    	$product_data['special_price_from_date'] = '';
    	$product_data['special_price_to_date'] = $product->getSpecialToDate();	
    	$product_data['url_key'] = $product->getUrlKey();
    	$product_data['meta_title'] = $product->getMetaTitle();
    	$product_data['base_image'] = $product->getImage();
    	$product_data['base_image_label'] = $product->getImageLabel();	
    	$product_data['small_image'] = $product->getSmallImage();
    	$product_data['small_image_label'] = $product->getSmallImageLabel();
    	$product_data['thumbnail_image'] =	$product->getThumbnail();
    	$product_data['thumbnail_image_label'] = $product->getThumbnailLabel();
    	$product_data['swatch_image'] =	'';
    	$product_data['swatch_image_label'] = '';	
    	$product_data['msrp_display_actual_price_type'] = '';
    	$product_data['country_of_manufacture'] = 'GB';
        $product_data['additional_attributes'] = '';  
    	if($product->getTypeId() === 'simple') {
            $product_data['additional_attributes'] = 'lens_power_reading='.$product->getAttributeText('power');
        }

        if($product->getTypeId() === 'configurable') {

            //List Your Custom Attributes as Variables:
            $mulilist = 'style,frametype_colour';

            $mulilistArr = explode(',', $mulilist);
            // Multi Selects
            $multiselects = array();
            foreach ($mulilistArr as $ms) {
               $multiselects[$ms] = $product->getAttributeText($ms);
            }

            $finalmultiArr = array();
            foreach ($multiselects as $mk => $mu) {
                  $_select = implode('|', $mu);
                  $_label = $mk;
                  $finalmultiArr[] .= $_label.'='.$_select;
            }

            $multiString = implode(',', $finalmultiArr);
            //Dropdowns


            $droplist = 'color,gender,is_imported,lens_type,manufacturer,power';
            
            $dropdownsArr = explode(',', $droplist);
            $dropdowns = array();
            foreach ($dropdownsArr as $ds) {
               $dropdowns[$ds] = $product->getAttributeText($ds);
            }
            $finaldropArr = array();
            foreach ($dropdowns as $dk => $du) {
                if($du) {
                  $_select = $du;
                  $_label = $dk;
                  $finaldropArr[] = $_label.'='.$_select;
                }

                  
            }
            $dropdownString = implode(',', $finaldropArr);
            

            $attributeString = '"'.$multiString.','.$dropdownString.'"';
         }

         $product_data['additional_attributes'] = $attributeString;

       

        $product_data['qty'] = $stock->getQty();

    	// Edit This Area || Or make it editable.
    	if($product->getTypeId() === 'configurable') {
    		$_simples = $product->getTypeInstance()->getUsedProducts($product);
    		//print_r($_simples);
    		$variations = array();
    		foreach ($_simples as $_simple) {
    		$item =  Mage::getModel('catalog/product')->load($_simple->getId());
    		$variations[] = 'sku='.$_simple->getSku().',lens_power_reading='.$item->getAttributeText('power');

    		}
    		$variationsString = implode('|', $variations);
 			$product_data['configurable_variations'] = '"'.$variationsString.'"';
    	} else {
    		$product_data['configurable_variations'] = '';
    	}

        //clean up data and put in quotes depending on field
        foreach ($product_data as $k => $val) {
            $bad = array('"', "\r\n", "\n", "\r", "\t",",");
            $good = array("", " ", " ", " ", ""," ");

            if ($k == 'description') {
                $product_data[$k] = str_replace($bad, $good, $val);

            } 

        }
        echo '<pre>';
        echo 'Processing product... ';
        print_r($product_data);
        echo '</pre>';

        //write line to the file
        $feed_line = implode(",", $product_data) . "\r\n";
        fwrite($handle, $feed_line);
        fflush($handle);

        //output line to browser in case running interactively

        
        echo '<p>';

        $counter++;
        // if ($counter==10) break;

    }
    echo 'Finished! - Processed ' . $counter . ' products';
    //---------------------- WRITE THE FEED
    fclose($handle);

} catch (Exception $e) {
    die($e->getMessage());
}

require_once('footer.php');
?>