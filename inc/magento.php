<?php 

define('SAVE_FEED_LOCATION', 'old_products.csv'); //you can set a new folder and file if you want, don't forget to chmod the folder to 777
// define source for Google Analytics
// make sure we don't time out or run out of memory
set_time_limit(0);
ini_set('memory_limit', '1024M');
ini_set('display_errors', 1);

$mageFilename = $_SERVER["DOCUMENT_ROOT"].'/app/Mage.php';
require_once $mageFilename;
Mage::app();

?>